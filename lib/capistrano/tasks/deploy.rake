namespace :deploy do
  desc "Deploy the site"

  task :pull do
    desc "Pull the latest image"
    on roles(:app) do
      execute "docker pull #{fetch :image_to_deploy}"
    end
  end

  task :copy do
    desc "Copy the files from the new image to the release_path"
    on roles(:app) do
      within(release_path) do
        container_id = capture("docker create #{fetch :image_to_deploy} echo")
        # make the top-level directory, so we can copy into it
        execute "mkdir #{release_path}/_dist"
        # copy built site into the `html` folder
        execute "docker cp #{container_id}:/site #{release_path}/_dist/html"
        # ensure permissions are correct
        execute "chmod -R o-rwX #{release_path}"
        # remove the created container, as we don't need it any more
        execute "docker rm #{container_id}"
      end
    end
  end

  after :updated, :pull
  after :pull, :copy
end
