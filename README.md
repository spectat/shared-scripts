# Shared Scripts for Spectat Sites

## Deployment

### Preparing the Git Repo

Pull this repo as a submodule:

```bash
git submodule add https://gitlab.com/spectat/shared-scripts .spectat/shared-scripts
```

First, to utilise the shared deployment scripts:

```bash
ln -s .spectat/shared-scripts/Capfile
mkdir -p lib
ln -s ../.spectat/shared-scripts/lib/capistrano lib/
mkdir -p config
```

Next, create a site-specific Capistrano configuration file, `config/deploy.rb`:

```ruby
set :application, 'www.spectatdesigns.co.uk'
set :full_repo_path, 'spectat/spectat/spectatdesigns.co.uk'
```

Create the configuration for production, `config/deploy/production.rb`:

```ruby
server 'www.spectatdesigns.co.uk', user: 'www-spectatdesigns-co-uk', roles: %w{app}
set :deploy_to, '/srv/www/www.spectatdesigns.co.uk'
```

Create the configuration for staging, `config/deploy/staging.rb`:

```ruby
server 'www.staging.spectatdesigns.co.uk', user: 'www-staging-spectatdesigns-co-uk', roles: %w{app}
set :deploy_to, '/srv/www/www.staging.spectatdesigns.co.uk'
```

Create the configuration for Review Apps, `config/deploy/review.rb`:

```ruby
server 'www.review.spectatdesigns.co.uk', user: 'www-review-spectatdesigns-co-uk', roles: %w{app}
set :deploy_to, "/srv/www/www.review.spectatdesigns.co.uk/#{fetch :tag}"
```
### Preparing GitLab Repositories

First, you need to grab the `deploy` SSH keys for the pipeline:

```
# for the variable `STAGING_SSH_PRIVATE_KEY`
ssh root@www.staging.spectatdesigns.co.uk 'cat ~www-staging-spectatdesigns-co-uk/.ssh/deploy'
# for the variable `REVIEW_SSH_PRIVATE_KEY`
ssh root@www.review.spectatdesigns.co.uk 'cat ~www-review-spectatdesigns-co-uk/.ssh/deploy'
```

Then place them into the [CI/CD](https://gitlab.com/spectat/spectat/spectatdesigns.co.uk/settings/ci_cd) section's `Secret variables` section.

Once complete, you will need to add read-only clone keys for the users:

```
# for the deploy key `www-staging-spectatdesigns-co-uk@01-staging`
ssh root@www.staging.spectatdesigns.co.uk 'cat ~www-staging-spectatdesigns-co-uk/.ssh/gitlab_ro.pub'
# for the deploy key `www-review-spectatdesigns-co-uk@01-staging`
ssh root@www.review.spectatdesigns.co.uk 'cat ~www-review-spectatdesigns-co-uk/.ssh/gitlab_ro.pub'
```

Place them into the [Repository](https://gitlab.com/spectat/spectat/spectatdesigns.co.uk/settings/repository) section's `Deploy Keys` section.
