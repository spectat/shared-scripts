require 'capistrano/setup'
require 'capistrano/deploy'

lock '~> 3.7.1'

require 'capistrano/scm/git'
install_plugin Capistrano::SCM::Git

# pick up any custom Capistrano tasks
Dir.glob('lib/capistrano/tasks/*.rake').each { |r| import r }

set :stages, ["staging", "production", "review"]
set :default_stage, "staging"

# `:full_repo_path` should be set within `config/deploy.rb`. Note that we use a
# lambda (`->{ }`) to lazy load the value, once other config files have been
# parsed
set :repo_url, ->{ "git@gitlab.com:#{fetch(:full_repo_path)}.git" }

# Branch for Capistrano checkout {{{
if ENV['CI_BUILD_REF_NAME']
  set :branch, ENV['CI_BUILD_REF_NAME']
else
  set :branch, 'master'
end
# }}}

# Docker registry {{{
set :registry_url, 'registry.gitlab.com'

set :tag, 'master'
set :tag, ENV['CI_COMMIT_REF_SLUG'] if ENV['CI_COMMIT_REF_SLUG']
if 'master' == fetch(:tag)
  if :production == fetch(:stage)
    set :tag, 'latest'
  elsif :staging == fetch(:stage)
    set :tag, 'master'
  end
end

# `:full_repo_path` should be set within `config/deploy.rb`. Note that we use a
# lambda (`->{ }`) to lazy load the value, once other config files have been
# parsed
set :image_to_deploy, ->{ "#{fetch(:registry_url)}/#{fetch(:full_repo_path)}:#{fetch(:tag)}" }
# }}}
